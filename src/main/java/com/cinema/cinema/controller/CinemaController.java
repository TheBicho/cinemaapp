package com.cinema.cinema.controller;

import com.cinema.cinema.dto.*;
import com.cinema.cinema.service.CinemaServ;
import com.cinema.cinema.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CinemaController {
    @Autowired
    private CinemaServ service;
    @Autowired
    private EmailService email;

    @RequestMapping(value="/register",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Cuenta registrarUsuario(@RequestBody Cuenta cuenta){
        return service.registrarUsuario(cuenta);
        //throw new ApiRequestException("xD");
    }
    @RequestMapping(value="/login",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Cuenta logearUsuario(@RequestBody Cuenta cuenta){
        return service.logearUsuario(cuenta);
    }

    @RequestMapping(value = "/movie",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Pelicula obtenerPelicula(@RequestBody Pelicula pelicula){
        return service.obtenerPelicula(pelicula);
    }

    @RequestMapping(value = "/email",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Boolean olvideContraseña(@RequestBody String cuenta){
        boolean confirm = false;
        Cuenta cuenta1 = service.olvideContraseña(cuenta);
        if(cuenta1!=null){
            confirm = true;
        }
        Email email1 = service.generarCorreo(cuenta1);
        email.sendEmail(email1);
        return confirm;
    }

    @RequestMapping(value = "/news",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Noticia obtenerNoticia(@RequestBody Noticia noticia){
        return service.obtenerNoticia(noticia);
    }
    @RequestMapping(value="/movies",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Peliculas obtenerPeliculas(@RequestBody Pelicula pelicula){
        return service.obtenerPeliculas(pelicula);
    }

    @RequestMapping(value="/seats",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Asientos obtenerAsientos(@RequestBody Sala sala){
        return service.obtenerAsientos(sala);
    }

    @RequestMapping(value="/upseats",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public Asientos almacenarAsientos(@RequestBody SalaAsientos salaAsientos){
        return service.almacenarAsientos(salaAsientos);
    }

}
