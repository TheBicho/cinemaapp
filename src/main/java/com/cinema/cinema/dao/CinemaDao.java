package com.cinema.cinema.dao;

import com.cinema.cinema.dto.*;

import java.util.List;

public interface CinemaDao {
    public Cuenta registrarUsuario(Cuenta cuenta);
    public Cuenta logearUsuario(Cuenta cuenta);
    public boolean validarTarjeta(TarjetaCred tarjetaCred);
    public Pelicula obtenerPelicula(Pelicula pelicula);
    public List<Imagen> obtenerPeliculas(Pelicula pelicula);
    public List<Asiento> obtenerAsientos(Sala sala);
    public void almacenarAsientos(List<Asiento> lista, Sala sala);
    public Cuenta olvideContraseña(String cuenta);
    public Email generarCorreo(Cuenta cuenta);
    public Noticia obtenerNoticia(Noticia noticia);
}
