package com.cinema.cinema.dao;

import com.cinema.cinema.dto.*;
import com.cinema.cinema.exceptions.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CinemaDaoImpl implements CinemaDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public Cuenta registrarUsuario(Cuenta cuenta){
        String SQL = "INSERT INTO users (usuario,contra, correo, nombre, apellido) VALUES (?,?,?,?,?)";
        try{
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.setString(1,cuenta.getUsuario());
            ps.setString(2,cuenta.getContrasenia());
            ps.setString(3,cuenta.getCorreo());
            ps.setString(4,cuenta.getNombres());
            ps.setString(5,cuenta.getApellidos());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException throwables) {
            String message;
            message = "EMAIL_USER_ERROR";
            throw new ApiRequestException(message,throwables);
        }
        return cuenta;

    }

    @Override
    public Cuenta logearUsuario(Cuenta cuenta){
        Cuenta log = null;
        boolean verif = false;
        String SQL = "SELECT usuario,contra,nombre,correo FROM users where usuario = ? and contra = ?";
        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.setString(1, cuenta.getUsuario());
            ps.setString(2, cuenta.getContrasenia());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                log = new Cuenta();
                log.setUsuario(rs.getString("usuario"));
                log.setContrasenia(rs.getString("contra"));
                log.setCorreo(rs.getString("correo"));
                log.setNombres(rs.getString("nombre"));
                verif=true;
            }
            if(!verif){
                throw new SQLException("ERROR");
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException throwables){
            String message;
            message = "LOGIN_ERROR";
            throw new ApiRequestException(message,throwables);
        }
        return log;
    }

    @Override
    public boolean validarTarjeta(TarjetaCred tarjetaCred) {

        return false;
    }

    @Override
    public Pelicula obtenerPelicula(Pelicula pelicula) {

        String SQL = "select sala, nombre, estreno, duracion, restriccion, director, reparto, productora, critica, sinopsis, genero, imagen, trailer from peliculas where id = ?";
        Pelicula pelicula1 = new Pelicula();

        try {

            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,pelicula.getId());
            ResultSet resultados = sentencia.executeQuery();

            while (resultados.next()) {
                pelicula1.setSala(resultados.getString("sala"));
                pelicula1.setNombre(resultados.getString("nombre"));
                pelicula1.setEstreno(resultados.getDate("estreno"));
                pelicula1.setDuracion(resultados.getInt("duracion"));
                pelicula1.setRestriccion(resultados.getString("restriccion"));
                pelicula1.setDirector(resultados.getString("director"));
                pelicula1.setReparto(resultados.getString("reparto"));
                pelicula1.setProductora(resultados.getString("productora"));
                pelicula1.setCritica(resultados.getString("critica"));
                pelicula1.setSinopsis(resultados.getString("sinopsis"));
                pelicula1.setGenero(resultados.getString("genero"));
                pelicula1.setImagen(resultados.getString("imagen"));
                pelicula1.setTrailer(resultados.getString("trailer"));
            }
            conexion.close();
            sentencia.close();
            resultados.close();

        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
        return pelicula1;
    }
    @Override
    public List<Imagen> obtenerPeliculas(Pelicula pelicula) {
        List<Imagen> lista = new ArrayList<>();
        String SQL = "select nombre, imagen, id, genero from peliculas where genero like ? order by nombre";

        try {

            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,"%"+pelicula.getGenero()+"%");
            ResultSet resultados = sentencia.executeQuery();

            while (resultados.next()) {
                Imagen imagen = new Imagen();
                imagen.setImagen(resultados.getString("imagen"));
                imagen.setId(resultados.getString("id"));
                imagen.setGenero(resultados.getString("genero"));
                lista.add(imagen);
            }
            conexion.close();
            sentencia.close();
            resultados.close();

        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
        return lista;
    }

    @Override
    public List<Asiento> obtenerAsientos(Sala sala){
        List<Asiento> lista = new ArrayList<>();
        String SQL = "SELECT codigo FROM "+sala.getCodigo();


        try {
            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = conexion.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);

            while (resultados.next()) {
                Asiento asiento1 = new Asiento();
                asiento1.setCodigo(resultados.getString("codigo"));
                lista.add(asiento1);
            }
            conexion.close();
            sentencia.close();
            resultados.close();

        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
        return lista;
    }
    @Override
    public void almacenarAsientos(List<Asiento> lista, Sala sala){
        String SQL = "INSERT INTO " +sala.getCodigo()+ " VALUES (?)";

        try {
            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            for(Asiento asiento: lista){
                sentencia.setString(1, asiento.getCodigo());
                sentencia.executeUpdate();
            }
            conexion.close();
            sentencia.close();

        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
    }

    @Override
    public Cuenta olvideContraseña(String cuenta) {
        Cuenta log = null;
        boolean verif = false;
        String SQL = "SELECT apellido,contra,nombre,correo FROM users where correo = ?";
        try {
            Connection con = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = con.prepareStatement(SQL);
            ps.setString(1, cuenta);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                log = new Cuenta();
                log.setNombres(rs.getString("nombre"));
                log.setApellidos(rs.getString("apellido"));
                log.setContrasenia(rs.getString("contra"));
                log.setCorreo(rs.getString("correo"));
                verif=true;
            }
            if(!verif){
                throw new SQLException("ERROR");
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException throwables){
            String message;
            message = "REQUEST_ERROR";
            throw new ApiRequestException(message,throwables);
        }
        return log;
    }

    @Override
    public Email generarCorreo(Cuenta cuenta) {
        Email correo = new Email();
        String mensaje = "Que tal " + cuenta.getNombres() +
                " " + cuenta.getApellidos() +
                "! se nos notifico que olvidaste tu contraseña " +
                "\nLa contraseña de su cuenta es: " + cuenta.getContrasenia() +
                ".\nProcure no olvidar su contraseña.\nTenga buen dia.";
        correo.setDestinatario(cuenta.getCorreo());
        correo.setMensaje(mensaje);
        correo.setAsunto("Recuperación de contraseña - CINE YA! ");
        return correo;
    }

    @Override
    public Noticia obtenerNoticia(Noticia noticia) {

        String SQL = "select titulo, imagen, subtitulo, infor from info_noti where clave = ?";
        Noticia noticia1 = new Noticia();

        try {

            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,noticia.getClave());
            ResultSet resultados = sentencia.executeQuery();

            while (resultados.next()) {
                noticia1.setTitulo(resultados.getString("titulo"));
                noticia1.setImagen(resultados.getString("imagen"));
                noticia1.setSubtitulo(resultados.getString("subtitulo"));
                noticia1.setInfor(resultados.getString("infor"));
            }
            conexion.close();
            sentencia.close();
            resultados.close();

        } catch (SQLException throwables) {

            throwables.printStackTrace();
        }
        return noticia1;
    }


}
