package com.cinema.cinema.exceptions;

public class ApiRequestException extends RuntimeException {
    public ApiRequestException(String message){
        super(message);
    }
    public ApiRequestException(String message, Throwable e){
        super(message,e);
    }
}
