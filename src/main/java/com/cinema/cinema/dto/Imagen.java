package com.cinema.cinema.dto;

import lombok.Data;

@Data
public class Imagen {
    private String imagen;
    private String id;
    private String genero;
}
