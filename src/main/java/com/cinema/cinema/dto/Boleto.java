package com.cinema.cinema.dto;


import lombok.Data;

@Data
public class Boleto {
    private String pelicula;
    private String sala;
    private String horario;
    private String entradas;
    private String totalCompra;

}
