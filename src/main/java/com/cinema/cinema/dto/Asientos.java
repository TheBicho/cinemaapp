package com.cinema.cinema.dto;

import lombok.Data;

import java.util.List;

@Data
public class Asientos {
    private List<Asiento> lista;
}
