package com.cinema.cinema.dto;

import lombok.Data;

@Data
public class Cuenta {

    private String usuario;
    private String contrasenia;
    private String correo;
    private String nombres;
    private String apellidos;

}
