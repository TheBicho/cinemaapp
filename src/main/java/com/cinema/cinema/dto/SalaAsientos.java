package com.cinema.cinema.dto;

import lombok.Data;

import java.util.List;

@Data
public class SalaAsientos {
    private List<Asiento> lista;
    private Sala sala;
}

