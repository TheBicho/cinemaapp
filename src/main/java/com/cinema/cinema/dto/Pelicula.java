package com.cinema.cinema.dto;

import lombok.Data;

import java.sql.Date;

@Data

public class Pelicula {

    private String nombre;
    private String sala;
    private Date estreno;
    private Integer duracion; //minutos
    private String restriccion;
    private String director;
    private String reparto;
    private String productora;
    private String critica;
    private String sinopsis;
    private String trailer;// url trailer
    private String imagen; // url imagen
    private String genero;
    private String id;

}
