package com.cinema.cinema.dto;

import lombok.Data;

@Data
public class Noticia {
    private String titulo;
    private String imagen;
    private String subtitulo;
    private String infor;
    private String clave;
}
