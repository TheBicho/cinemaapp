package com.cinema.cinema.dto;

import lombok.Data;

@Data
public class TarjetaCred {
    private Integer numcard;
    private String nombres_apellidos;
    private Integer fechaExpira;
    private Integer codigoSeguridad;
    private Integer dni;
}
