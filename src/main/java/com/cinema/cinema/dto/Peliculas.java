package com.cinema.cinema.dto;

import lombok.Data;

import java.util.List;

@Data
public class Peliculas {
    private List<Imagen> lista;
}
