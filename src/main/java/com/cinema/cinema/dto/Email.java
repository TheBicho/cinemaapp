package com.cinema.cinema.dto;

import lombok.Data;

@Data
public class Email {
    private String destinatario;
    private String asunto;
    private String mensaje;
}
