package com.cinema.cinema.service;

import com.cinema.cinema.dao.CinemaDao;
import com.cinema.cinema.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CinemaServImpl implements CinemaServ{
    @Autowired
    private CinemaDao dao;
    @Override
    public Cuenta registrarUsuario(Cuenta cuenta) {
        return dao.registrarUsuario(cuenta);
    }

    @Override
    public Cuenta logearUsuario(Cuenta cuenta){
        return dao.logearUsuario(cuenta);
    }

    @Override
    public boolean validarTarjeta(TarjetaCred tarjetaCred) {
        return dao.validarTarjeta(tarjetaCred);
    }

    @Override
    public Pelicula obtenerPelicula(Pelicula pelicula) {
        return dao.obtenerPelicula(pelicula);
    }

    @Override
    public Peliculas obtenerPeliculas(Pelicula pelicula) {
        Peliculas lista = new Peliculas();
        lista.setLista(dao.obtenerPeliculas(pelicula));
        return lista;
    }

    @Override
    public Asientos obtenerAsientos(Sala sala){
        Asientos asientos = new Asientos();
        asientos.setLista(dao.obtenerAsientos(sala));
        return asientos;
    }

    @Override
    public Asientos almacenarAsientos(SalaAsientos salaAsientos) {
        Asientos asientos = new Asientos();
        asientos.setLista(salaAsientos.getLista());
        dao.almacenarAsientos(salaAsientos.getLista(), salaAsientos.getSala());
        return asientos;
    }

    @Override
    public Cuenta olvideContraseña(String cuenta) {
        return dao.olvideContraseña(cuenta);
    }

    @Override
    public Email generarCorreo(Cuenta cuenta) {
        return dao.generarCorreo(cuenta);
    }
    @Override
    public Noticia obtenerNoticia(Noticia noticia){ return dao.obtenerNoticia(noticia); }
}
