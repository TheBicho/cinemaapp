package com.cinema.cinema.service;

import com.cinema.cinema.dto.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    public void sendEmail(Email correo) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom("soportecineya@gmail.com");
        email.setTo(correo.getDestinatario());
        email.setSubject(correo.getAsunto());
        email.setText(correo.getMensaje());
        /*
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try{
            helper.setTo(correo.getDestinatario());
            helper.setText(correo.getMensaje());
            helper.setSubject(correo.getAsunto());
        }catch (MessagingException e){
            e.printStackTrace();
        }*/

        mailSender.send(email);
    }
}
