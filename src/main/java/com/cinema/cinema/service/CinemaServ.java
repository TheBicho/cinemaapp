package com.cinema.cinema.service;

import com.cinema.cinema.dto.*;

public interface CinemaServ {
    public Cuenta registrarUsuario(Cuenta cuenta);
    public Cuenta logearUsuario(Cuenta cuenta);
    public boolean validarTarjeta(TarjetaCred tarjetaCred);
    public Pelicula obtenerPelicula(Pelicula pelicula);
    public Peliculas obtenerPeliculas(Pelicula pelicula);
    public Asientos obtenerAsientos(Sala sala);
    public Asientos almacenarAsientos(SalaAsientos salaAsientos);
    public Cuenta olvideContraseña(String cuenta);
    public Email generarCorreo(Cuenta cuenta);
    public Noticia obtenerNoticia(Noticia noticia);
}
