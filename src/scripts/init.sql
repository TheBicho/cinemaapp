CREATE TABLE info_noti (

                           titulo VARCHAR(50),
                           imagen VARCHAR(30),
                           subtitulo VARCHAR(120),
                           clave VARCHAR(15),
                           infor VARCHAR(1000)
);

INSERT INTO info_noti
VALUES('LAS MEJORES PELÍCULAS EN ESTE 2020', '/assets/noticias/mejor.jpg', 'Entérate cuáles fueron las películas que resaltaron este año.', 'mejor',
       'Estas son las mejores películas que hemos visto este año: El faro, estupenda segunda película de Robert Eggers, que ya tomó al asalto el cine de terror hace unos
       años con la magistral; por otro lado tenemos a la aclamada 1917, la primera película de Sam Mendes tras abandonar la franquicia de James Bond fue una de las más aclamadas
        por la crítica durante este año, y ganó tres Oscars técnicos (entre ellos, muy merecidamente, a la extraordinaria fotografía de Roger Deakins) y Globos de Oro a mejor
         drama y mejor director, y cerramos esta lista con Uncut gems, el cual posee un magnetismo impensable para tratarse de un filme que te golpea una y otra vez sin ningún
           tipo de piedad a cada revés que sufre su protagonista. Una deliciosa tortura que impide apartar la mirada, filmada con un nervio próximo al caos ordenado.');


INSERT INTO info_noti
VALUES('CRÍTICA Y ANÁLISIS DE RETABLO', '/assets/noticias/retablo.jpg', 'La historia de amor y tolerancia que necesitábamos', 'retablo',
       '‘Retablo’, la ópera prima de Álvaro Delgado-Aparicio, y la primera película peruana en ser nominada a un premio Independent Spirit y un BAFTA, es un film narrado
        en quechua que nos invita a reflexionar sobre la silenciosa batalla que luchamos contra los lazos de dependencia que nos rodean. Bajo la perspectiva de Segundo
        (Junior Bejar Roca), un adolescente de 14 años, la película nos acerca a la cosmovisión andina y a sus juicios sobre la masculinidad, las crisis de identidad y los
         roles de género fuertemente arraigados en una sociedad olvidada, casi inexistente para un Estado completamente centralizado.');


INSERT INTO info_noti
VALUES('PELÍCULAS LLENAS DE MAGIA', '/assets/noticias/magia.jpg', 'Continua la maratón con estas películas llenas de magia.', 'magia',
       'Presta atención a estas películas que hoy te recomendamos: El señor de los anillos, Frodo es un hobbit que vive tranquilamente hasta que se ve involucrado en una misión
        peligrosa para destruir un poderoso anillo y, junto con varios guerreros y un mago, emprenderá esta aventura, además también te sugerimos: Las crónicas de Narnia: el león,
        la bruja y el ropero, ¿imaginas que tu ropero sea la puerta a otro mundo? En esta película cuatro hermanos descubren una puerta a Narnia, un reino que está dominado por una
        malvada bruja y animales que hablan. Finalmente, culmina tu maraton con La brújula dorada, en un mundo donde las almas humanas se presentan como animales llamados daimonion,
        una pequeña huérfana llamada Lyra se verá involucrada en una lucha contra un grupo de personas que buscan hacer experimentos en niños para arrebatarles sus daimonions.');


INSERT INTO info_noti
VALUES('CONOCIENDO A BONG JOON-HO', '/assets/noticias/bong.jpg', 'Lleva casi dos décadas siendo uno de los mejores cineastas del mundo.', 'bong',
       'Bong Joon-ho hizo historia en los Oscars pero lo cierto es que lleva haciéndola bastante más tiempo. Sí, ‘Parásitos’ es la primera película de habla no inglesa en ganar el
       Premio a Mejor Película. Sí, sus cuatro Oscars son los primeros que logra Corea del sur. Sí, ninguna película de habla no inglesa había ganado el SAG Awards a Mejor Reparto.
        Pero es que ya hace casi un año que comenzó su recorrido en Cannes, consiguiendo también la primera Palma de Oro para el país asiático. Con su película anterior, ‘Okja’, apareció
         por primera vez el logo de Netflix en el festival francés, fue la primera edición que permitió entrar a concurso a las producciones de la plataforma, y también la última. Con
         ‘The Host’ insufló nueva vida al cine de monstruos, siempre mejor con sello asiático desde el nacimiento de Godzilla.');

CREATE TABLE users(
                      nombre VARCHAR(20) ,
                      apellido VARCHAR(20),
                      usuario VARCHAR(20) UNIQUE,
                      contra VARCHAR(20),
                      correo VARCHAR(40) UNIQUE
);
INSERT INTO users
VALUES ('Aarom Sebastian','Mendoza Altamirano','sm040101','M3254538A','sm040101@gmail.com');
CREATE TABLE peliculas(

                          nombre VARCHAR(35),
                          estreno DATE,
                          duracion INT,
                          restriccion VARCHAR(5),
                          director VARCHAR(20),
                          reparto VARCHAR(80),
                          productora VARCHAR(50),
                          critica VARCHAR(3),
                          sinopsis VARCHAR(1000),
                          trailer VARCHAR(50),
                          imagen VARCHAR(120),
                          genero VARCHAR(40),
                          id VARCHAR(15),
                          sala VARCHAR(7)
);

INSERT INTO peliculas
VALUES ('WALL-E', "2008-07-24", 103, 'APT', 'Andrew Stanton', 'Ben Burtt, Elissa Knight, Jeff Garlin', 'Pixar Animation Studios',
        '90%', 'Por culpa de la contaminación, los humanos abandonaron la Tierra dejando atrás miles de robots encargados de su limpieza. Pero la programación de las máquinas falló, a excepción del pequeño Wall-E, que lleva 700 años dedicado a recoger basura. Un día conoce a una elegante robot de búsqueda llamada EVE, que descubre que Wall-E ha dado con la clave para el futuro de la Tierra. Cuando EVE regresa al espacio para informar de su hallazgo a los humanos, Wall-E persigue a su nueva amiga por la galaxia viviendo mil aventuras.',
        'https://www.youtube.com/embed/4rDD3SccHxQ', '/assets/img/walle.jpg',
        'Infantil, Ciencia ficción', 'walle', 'sala1');


INSERT INTO peliculas
VALUES ('Buscando a Nemo', "2003-07-03", 100, 'APT', 'Andrew Stanton', 'Albert Brooks, Ellen DeGeneres, Alexander Gould', 'Pixar Animation Studios',
        '86%', 'En esta impresionante aventura submarina, con personajes memorables, humor y emoción sincera, Buscando a Nemo sigue el viaje cómico y trascendental de un pez payaso demasiado protector llamado Marlin y su hijo Nemo, quienes se convierten en separados en la Gran Barrera de Coral cuando Nemo es llevado inesperadamente lejos de su hogar en el océano y arrojado a una pecera en la oficina de un dentista. Animado por la compañía de Dory, un amistoso pero olvidadizo sabor azul real del Pacífico, Marlin se embarca en una peligrosa travesía y se convierte en el héroe improbable de un esfuerzo épico para rescatar a su hijo, que incuba algunos sus propios planes atrevidos para regresar sano y salvo a casa.',
        'https://www.youtube.com/embed/X-Z-gQ8mKs0', '/assets/img/nemo.jpg',
        'Aventura, Infantil', 'nemo', 'sala2');


INSERT INTO peliculas
VALUES ('Los Increíbles', "2004-12-25", 116, 'APT', 'Brad Bird', 'Holly Hunter, Craig T. Nelson, Samuel L. Jackson', 'Pixar Animation Studios',
        '75%', 'Bob Parr, uno de los principales luchadores enmascarados contra el crimen del mundo, conocido por todos como "Mr. Increíble", luchó contra el mal y salvó vidas a diario. Pero ahora, quince años después, Bob y su esposa, un famoso superhéroe por derecho propio, han adoptado identidades civiles y se han retirado a los suburbios para vivir una vida normal con sus tres hijos. Ahora es un ajustador de reclamos de seguros que lucha contra el aburrimiento y una cintura abultada. Con ganas de volver a la acción, Bob tiene su oportunidad cuando una misteriosa comunicación lo convoca a una isla remota para una misión de alto secreto.',
        'https://www.youtube.com/embed/eHwtl05KOgo', '/assets/img/increibles.jpg',
        'Acción, Infantil', 'increibles', 'sala3');


INSERT INTO peliculas
VALUES ('Shrek', "2001-05-18", 95, 'APT', 'Andrew Adamson', 'Mike Myers, Eddie Murphy, Cameron Diaz', 'DreamWorks Animation',
        '90%', 'Érase una vez, en un pantano lejano, vivía un ogro malhumorado llamado Shrek cuya preciosa soledad es repentinamente destrozada por una invasión de molestos personajes de cuentos de hadas. Hay ratones ciegos en su comida, un lobo grande y feroz en su cama, tres cerditos sin hogar y más, todos desterrados de su reino por el malvado Lord Farquaad. Decidido a salvar su hogar, por no mencionar el suyo, Shrek hace un trato con Farquaad y se propone rescatar a la hermosa princesa Fiona para que sea la novia de Farquaad. Acompañarlo en su misión es Burro bromista, que hará cualquier cosa por Shrek ... excepto callarse. Rescatar a la princesa de un dragón que escupe fuego puede resultar el menor de sus problemas cuando se revele el profundo y oscuro secreto que ha estado guardando.',
        'https://www.youtube.com/embed/XvWGGwSuq8I', '/assets/img/shrek.jpg',
        'Infantil, Comedia', 'shrek', 'sala4');


INSERT INTO peliculas
VALUES ('Annabelle', "2014-10-23", 99, '+18', 'John R. Leonetti', 'Annabelle Wallis, Ward Horton, Tony Amendola', 'Warner Bros.',
        '36%', 'John Form encuentra el regalo perfecto para su mujer embarazada, Mia: una preciosa e inusual muñeca vintage que lleva un vestido de novia blanco inmaculado. Sin embargo, la alegría de Mia al recibir a Annabelle no dura mucho. Durante una espantosa noche la pareja ve como miembros de una secta satánica invaden su hogar y los atacan brutalmente. No sólo dejan sangre derramada y terror tras su visita…los miembros de la secta conjuran a un ente de tal maldad que nada de lo que han hecho se compara al siniestro camino a la maldición que ahora es… Annabelle.',
        'https://www.youtube.com/embed/8kmLHwKH31M', '/assets/img/annabelle.jpg',
        'Terror, Suspenso', 'annabelle', 'sala5');


INSERT INTO peliculas
VALUES ('¡Huye!', "2017-02-24", 104, '+18', 'Jordan Peele', 'Daniel Kaluuya, Allison Williams, Bradley Whitford', 'Monkeypaw Productions',
        '86%', 'Ahora que Chris y su novia, Rose, han alcanzado el hito de las citas de conocer a los padres, ella lo invita a una escapada de fin de semana al norte del estado con Missy y Dean. Al principio, Chris interpreta el comportamiento excesivamente complaciente de la familia como intentos nerviosos de lidiar con la relación interracial de su hija, pero a medida que avanza el fin de semana, una serie de descubrimientos cada vez más inquietantes lo llevan a una verdad que nunca podría haber imaginado.',
        'https://www.youtube.com/embed/xGhKr2uNkI4', '/assets/img/huye.jpg',
        'Terror, Suspenso', 'huye', 'sala6');


INSERT INTO peliculas
VALUES ('Interestellar', "2014-11-06", 169, '+14', 'Christopher Nolan', 'Matthew McConaughey, Anne Hathaway, Jessica Chastain', 'Paramount Pictures',
        '86%', 'Al ver que la vida en la Tierra está llegando a su fin, un grupo de exploradores dirigidos por el piloto Cooper y la científica Amelia emprende una misión que puede ser la más importante de la historia de la humanidad: viajar más allá de nuestra galaxia para descubrir algún planeta en otra que pueda garantizar el futuro de la raza humana.',
        'https://www.youtube.com/embed/NqniWGlg5kU', '/assets/img/interestellar.jpg',
        'Ciencia ficción, Aventura', 'interestellar', 'sala7');


INSERT INTO peliculas
VALUES ('Una aventura extraordinaria', "2012-12-27", 127, '+14', 'Ang Lee', 'Suraj Sharma, Irrfan Khan, Tabu', 'Rhythm & Hues',
        '84%', 'Después de decidir vender su zoológico en la India y mudarse a Canadá, Santosh y Gita Patel viajan en un barco carguero con sus hijos y algunos animales. Una terrible tormenta hunde el barco, dejando a Pi, el hijo de los Patel, como el único sobreviviente humano. Sin embargo, Pi no está solo; un temible tigre de bengala lo acompaña en el bote salvavidas. Los días se vuelven semanas y meses, y Pi y el tigre deben aprender a confiar el uno en el otro para sobrevivir.',
        'https://www.youtube.com/embed/wcCRHCjD55o', '/assets/img/aventura.jpg',
        'Drama, Aventura', 'aventura', 'sala8');


INSERT INTO peliculas
VALUES ('El niño con el pijama de rayas', "2008-04-06", 94, '+14', 'Mark Herman', 'Asa Butterfield, Vera Farmiga, David Thewlis', 'Heyday Films',
        '85%', 'Bruno es un niño de 9 años que se ve obligado a abandonar Berlín y dejar atrás a sus amigos cuando su padre, un oficial nazi, recibe el mando del campo de concentración de Auschwitz. Con su inocente personalidad, Bruno no entiende qué ocurre en el campo de concentración ni por qué ahí dentro todo el mundo viste de forma tan rara. Pronto la curiosidad le vence y comienza a explorar los límites del campo de concentracaión siempre que puede. Durante una de sus escapadas, Bruno conoce a un niño llamado Shmuel (Jack Scanlon), un niño judío que se encuentra al otro lado de la valla, vestido con un curioso pijama de rayas.',
        'https://www.youtube.com/embed/hd028fGZdG8', '/assets/img/pijama.jpg',
        'Drama, Tragedia', 'pijama', 'sala9');


INSERT INTO peliculas
VALUES ('Sin escape', "2015-09-03", 103, '+14',	'John Erick Dowdle', 'Owen Wilson, Lake Bell, Pierce Brosnan', 'Bold Films',
        '62%', 'Un intenso thriller internacional, se centra en un empresario estadounidense mientras él y su familia se instalan en su nuevo hogar en el sudeste asiático. De repente, al encontrarse en medio de un violento levantamiento político, deben buscar frenéticamente un escape seguro mientras los rebeldes atacan sin piedad la ciudad.',
        'https://www.youtube.com/embed/rxq_JyMEzPs', '/assets/img/escape.jpg',
        'Suspenso, Acción', 'escape', 'sala10');


INSERT INTO peliculas
VALUES ('Búsqueda implacable', "2008-09-05", 93, '+14', 'Pierre Morel', 'Liam Neeson, Maggie Grace, Framke Janssen', 'EuropaCorp',
        '85%', 'Bryan Mills es un agente especial jubilado. Pero cuando su hija Kim es secuestrada en París por una organización criminal albanokosovar, tendrá que volver a la acción para intentar salvarla. La banda se dedica a una red de trata de blancas, por lo que Mills sabe que sólo dispone de unas horas para conseguir rescatarla antes de que se pierda el rastro de su hija.',
        'https://www.youtube.com/embed/mce-GkAuq20', '/assets/img/implacable.jpg',
        'Suspenso, Acción', 'implacable', 'sala11');


INSERT INTO peliculas
VALUES ('Agentes del desorden', "2014-08-13", 104, '+14', 'Luke Greenfield', 'Damon Wayans Jr., Jake Johnson, Nina Dobrev', '20th Century Fox',
        '51%', 'Es la mejor película de policías amigos excepto por una cosa: no son policías. Cuando dos amigos que luchan se visten de policías para una fiesta de disfraces, se convierten en sensaciones del vecindario. Pero cuando estos nuevos "héroes" se enredan en una red de mafiosos y detectives sucios de la vida real, deben arriesgar sus insignias falsas.',
        'https://www.youtube.com/embed/XSqX_dMmb9g', '/assets/img/agentes.jpg',
        'Comedia, Acción', 'agentes', 'sala12');

INSERT INTO peliculas
VALUES ('Manchester junto al mar', "2016-11-14", 137, '+14', 'Kenneth Lonergan', 'Casey Affleck, Michelle Williams, Lucas Hedges', 'Roadside Attractions',
        '78%', 'Después de la muerte de su hermano mayor Joe, Lee Chandler se sorprende de que Joe lo haya convertido en el único guardián de su sobrino adolescente Patrick. Tras despedirse de su trabajo como conserje en Boston, Lee regresa a regañadientes a Manchester-by-the-Sea, el pueblo de pescadores donde su familia de clase trabajadora ha vivido durante generaciones. Allí, se ve obligado a lidiar con un pasado que lo separó de su esposa, Randi , y de la comunidad donde nació y se crió.',
        'https://www.youtube.com/embed/CPvXci8-szY', '/assets/img/manchester.jpg', 'Tragedia, Drama', 'manchester', 'sala13');

INSERT INTO peliculas
VALUES ('¿Qué le pasó a lunes?', "2017-08-18", 124, '+14', 'Tommy Wirkola', 'Noomi Rapace, Willem Dafoe, Glen Cose',	'Raffaella De Laurentiis',
        '67%', 'En un futuro distópico en el que la sobrepoblación y la hambruna han obligado al gobierno a implantar una política de un único hijo, siete hermanas, cada una con el nombre de un día de la semana, luchan por sobrevivir y pasar inadvertidas haciéndose pasar por una sola persona cuando salen a la calle: Karen Settman. Pero un día una de ella, Lunes, desaparece sin dejar rastro. Las otras hermanas intentarán encontrarla.',
        'https://www.youtube.com/embed/sRIYLZJqbEY', '/assets/img/lunes.jpg', 'Ciencia ficción, Acción', 'lunes', 'sala14');

INSERT INTO peliculas
VALUES ('Green book', "2018-11-21",	130, '+14', 'Peter Farelly', 'Mahershala Ali, Viggo Mortensen, Nick Vallelonga', 'DreamWorks	Pictures',
        '91%', 'El Dr. Don Shirley es un pianista afroamericano de talla mundial que está a punto de embarcarse en una gira de conciertos en el sur profundo en 1962. Necesitando un conductor y protección, Shirley recluta a Tony Lip, un portero de habla dura de un italiano. Barrio americano en el Bronx. A pesar de sus diferencias, los dos hombres pronto desarrollan un vínculo inesperado al enfrentar el racismo y el peligro en una era de segregación.',
        'https://www.youtube.com/embed/eQQp_tXjP2U', '/assets/img/green.jpg', 'Comedia, Drama', 'green', 'sala15');

INSERT INTO peliculas
VALUES ('Bird box', "2018-12-14", 126, '+14', 'Susanne Bier', 'Sandra Bullock, Trevante Rhodes, Sarah Paulson', 'Bluegrass Films',
        '57%', 'Cuando una fuerza misteriosa diezma a la población, solo una cosa es segura: si la ves, mueres. Los supervivientes ahora deben evitar encontrarse cara a cara con una entidad que toma la forma de sus peores miedos. En busca de esperanza y un nuevo comienzo, una mujer y sus hijos se embarcan en un peligroso viaje a través del bosque y río abajo para encontrar el único lugar que pueda ofrecer refugio. Para lograrlo, tendrán que taparse los ojos del mal que los persigue y completar el viaje con los ojos vendados.',
        'https://www.youtube.com/embed/4vFsD_1BFLs', '/assets/img/box.jpg', 'Terror, Ciencia ficción', 'box', 'sala16');

INSERT INTO peliculas
VALUES ('Dos policías rebeldes', "1995-04-07", 119, '+14', 'Michael Bay', 'Will Smith, Martin Lawrence, Joe Pantoliano', 'Columbia pictures',
        '96%', 'La esposa y el hijo de un narcotraficante mexicano se embarcan en una búsqueda vengativa para matar a todos los involucrados en su juicio y encarcelamiento, incluido el detective de Miami Mike Lowrey. Cuando Mike resulta herido, se une a su compañero Marcus Burnett y AMMO, un escuadrón táctico especial, para llevar a los culpables ante la justicia. Pero los policías bromistas de la vieja escuela pronto deben aprender a llevarse bien con sus nuevos homólogos de élite si quieren acabar con el cartel vicioso que amenaza sus vidas.',
        'https://www.youtube.com/embed/91Y6AFxv_rA', '/assets/img/rebeldes.jpg', 'Comedia, Acción', 'rebeldes', 'sala17');

INSERT INTO peliculas
VALUES ('Hotel Mumbai', "2019-03-14", 123, '+18', 'Anthony Maras', 'Dev Patel, Nazanin Boniadi, Armie Hammer', 'Thunder Road Pictures',
        '86%', 'El largometraje recrea el ataque terrorista que tuvo lugar en 2008 en el hotel Taj Mahal de Mumbai (India), cuando un grupo de pakistaníes armados retuvieron durante 68 horas a cientos de personas, de las que murieron más de 160. Gracias a la valiente reacción del personal del hotel y de sus clientes se pudo prevenir una catástrofe mayor.',
        'https://www.youtube.com/embed/td03ewyR9LI', '/assets/img/hotel.jpg', 'Tragedia, Drama', 'hotel', 'sala18');

INSERT INTO peliculas
VALUES ('Boda sangrienta', "2019-08-22", 94, '+18', 'Matt Bettinelli', 'Samara Weaving, Adam Brody, Andie MacDowell', 'Searchlight Pictures',
        '78%', 'Grace no podría estar más feliz después de casarse con el hombre de sus sueños en la lujosa propiedad de su familia. Solo hay una trampa: ahora debe esconderse desde la medianoche hasta el amanecer mientras sus nuevos suegros la persiguen con pistolas, ballestas y otras armas. Mientras Grace intenta desesperadamente sobrevivir a la noche, pronto encuentra una manera de darle la vuelta a sus parientes no tan adorables.',
        'https://www.youtube.com/embed/MHt5G2P7KM0', '/assets/img/boda.jpg', 'Terror, Suspenso', 'boda', 'sala19');

INSERT INTO peliculas
VALUES ('22 de julio', "2018-10-10", 143, '+18', 'Paul Greengrass', 'Anders Danielsen, Jonas Strand, Seda Witt', 'Scott Rudin Productions',
        '70%', 'Narra el atentado terrorista más letal de la historia de Noruega y los sucesos posteriores. El 22 de julio de 2011, un ultraderechista radical detonó un coche bomba en Oslo y luego disparó a los adolescentes de un campamento de verano en la isla de Utøya. Murieron 77 personas. A través de los ojos de un superviviente, y en paralelo a su recuperación física y emocional, "22 de julio" retrata la trayectoria del país para lograr su curación y reconciliación.',
        'https://www.youtube.com/embed/J5uomLlwbHY', '/assets/img/julio.jpg', 'Tragedia, Drama', 'julio', 'sala20');

INSERT INTO peliculas
VALUES ('Sonic', "2020-02-09", 100, 'APT', 'Jeff Fowler', 'Jim Carrey, Ben Schwartz, James Marsden', 'Original Film',
        '93%', 'El mundo necesitaba un héroe, tenía un erizo. Impulsado con una velocidad increíble, Sonic abraza su nuevo hogar en la Tierra, hasta que accidentalmente golpea la red eléctrica, lo que despierta la atención del genio malvado Dr. Robotnik. Ahora, es supervillano contra supersónico en una carrera sin cuartel por todo el mundo para evitar que Robotnik use el poder único de Sonic para lograr la dominación mundial.',
        'https://www.youtube.com/embed/OGca96afgtM', '/assets/img/sonic.jpg', 'Infantil, Comedia', 'sonic', 'sala21');

INSERT INTO peliculas
VALUES ('El corredor del laberinto', "2014-09-11", 113, '+14', 'Wes Ball', 'Kaya Scodelario, Patricia Clarkson, Thomas Brodie-Sangster', 'Gotham Group',
        '68%', 'Thomas, un adolescente, llega a un claro en el centro de un laberinto gigante. Al igual que los otros jóvenes arrojados allí antes que él, no recuerda su vida anterior. Thomas se convierte rápidamente en parte del grupo y poco después demuestra una perspectiva única que le otorga un ascenso al estado de corredor: aquellos que patrullan el laberinto en constante cambio para encontrar una ruta de escape. Junto con Teresa, la única mujer, Thomas intenta convencer a sus cohortes de que conoce una salida.',
        'https://www.youtube.com/embed/KNYPRt6SfrE', '/assets/img/corredor.jpg', 'Ciencia ficción, Acción', 'corredor', 'sala22');

INSERT INTO peliculas
VALUES ('El exorcismo de Emily Rose', "2005-09-09", 122, '+18', 'Scott Derrickson', 'Jennifer Carpenter, Laura Linney, Tom Wilkinson', 'Lakeshore Entertainment',
        '60%', 'El reverendo Moore es procesado por la muerte injusta de una niña que se cree que está poseída por demonios, porque administró el exorcismo sancionado por la iglesia que finalmente la mató. El abogado de la acusación Ethan Thomas sostiene que la joven, Emily, sufría de esquizofrenia y debería haber sido diagnosticada médicamente. Mientras tanto, la abogada defensora Erin Bruner argumenta que la condición de Emily no puede explicarse solo por la ciencia.',
        'https://www.youtube.com/watch?v=021-tHmxjEY', '/assets/img/exorcismo.jpg', 'Terror, Suspenso', 'exorcismo', 'sala23');

INSERT INTO peliculas
VALUES ('Lo imposible', "2013-01-01", 114, '+14', 'Juan Antonia Bayona', 'Naomi Watts, Ewan McGregor, Geraldine Chaplin', 'Apaches Entertainment',
        '84%', 'En diciembre de 2004, la familia unida Maria, Henry y sus tres hijos comienzan sus vacaciones de invierno en Tailandia. Pero el día después de Navidad, la idílica fiesta se convierte en una pesadilla incomprensible cuando un rugido aterrador se eleva desde las profundidades del mar, seguido de un muro de agua negra que devora todo a su paso. Aunque María y su familia enfrentan su hora más oscura, las inesperadas muestras de bondad y coraje alivian su terror.',
        'https://www.youtube.com/embed/JHldZNvZY4Q', '/assets/img/imposible.jpg', 'Tragedia, Drama', 'imposible', 'sala24');
INSERT INTO peliculas
VALUES ('Black Adam', "2021-12-22", 123, '+14', 'Jaume Collet-Serra', 'Dwayne Johnson, Noah Centineo', 'DC Comics',
        '84%', 'Película sobre Black Adam (Dwayne Johnson), uno de los supervillanos más peligrosos pertenecientes al Universo de DC Comics, ya que no hay ningún héroe que lo haya superado, ni siquiera el mismísimo Superman. Además, cuenta con una experiencia de miles de años, pues su origen se encuentra en el antiguo Egipto, alrededor del año 1200 a.C., donde fue sumo sacerdote. Tiene diversos poderes, como la inmortalidad o la fuerza y velocidad sobrehumanas.',
        'https://www.youtube.com/watch?v=j6x3Aeqtabc', '/assets/img/adam.jpg', 'Tragedia, Drama', 'adam', '');


INSERT INTO peliculas
VALUES ('Avatar 2', "2021-12-01", 175, '+14', '	James Cameron', 'Sam Worthington, Zoe Saldana, Sigourney Weaver, Stephen Lang', '20th Century Studios',
        '84%', 'En diciembre de 20021, la familia unida Maria, Henry y sus tres hijos comienzan sus vacaciones de invierno en Tailandia. Pero el día después de Navidad, la idílica fiesta se convierte en una pesadilla incomprensible cuando un rugido aterrador se eleva desde las profundidades del mar, seguido de un muro de agua negra que devora todo a su paso. Aunque María y su familia enfrentan su hora más oscura, las inesperadas muestras de bondad y coraje alivian su terror.',
        'https://www.youtube.com/watch?v=AxLH0lXEGAY', '/assets/img/avatar.jpg', 'Ciencia ficción, Aventura', 'avatar', '');


INSERT INTO peliculas
VALUES ('Cruella', "2021-05-28", 110, '+14', 'Craig Gillespie', 'Emma Stone, Emma Thompson, Paul Walter, Joel Fry', 'Walt Disney Pictures',
        '84%', 'Ambientada en la década de 1970 en Londres, la diseñadora de moda Cruella de Vil se obsesiona con las pieles de los perros, especialmente los dálmatas hasta que se convierte en una leyenda despiadada y aterradora.',
        'https://www.youtube.com/watch?v=KnAHNmaTs94', '/assets/img/cruella.jpg', 'Comedia, Aventura', 'cruella', '');


INSERT INTO peliculas
VALUES ('Jurassic World', "2021-06-11", 165, '+14', 'Joe Johnston', 'Sam Neill, William H. Macy, Téa Leoni, Alessandro Nivola', 'Legendary Pictures',
        '84%', 'Cuando la joven Maisie es secuestrada por cazadores furtivos de dinosaurios. Owen y Claire se dispusieron a buscarla y rescatarla. Su viaje los llevara a un hábitat de dinosaurios operado por una corporación global con una agenda posiblemente siniestra … eso incluso ahora está siendo investigado por Alan Grant y Ellie Sattler.',
        'https://www.youtube.com/watch?v=lVYwe2htD-s', '/assets/img/jworld.jpg', 'Ciencia ficción, Terror', 'jworld', '');


INSERT INTO peliculas
VALUES ('Black Panther 2', "2022-05-06", 154, '+14', 'JRyan Coogler', 'Letitia Wright, Danai Gurira, Lupita Nyong´o, Andy Serkis', 'Marvel Studios',
        '84%', 'Tras los hechos sucedidos en Vengadores: Endgame, T´Challa/Black Panther regresa a una Wakanda que ha cambiado tras la batalla contra el Ejército de Thanos en Vengadores: Infinity War.',
        'https://www.youtube.com/watch?v=SfEJruPT5Zw', '/assets/img/panther.jpg', 'Tragedia, Drama', 'panther', '');

CREATE TABLE sala1(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala1 (codigo) VALUES ('H4');

CREATE TABLE sala2(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala2 (codigo) VALUES ('F1');

CREATE TABLE sala3(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala3 (codigo) VALUES ('D8');

CREATE TABLE sala4(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala4 (codigo) VALUES ('B2');

CREATE TABLE sala5(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala5(codigo) VALUES ('H10');

CREATE TABLE sala6(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala6 (codigo) VALUES ('A10');

CREATE TABLE sala7(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala7 (codigo) VALUES ('F9');

CREATE TABLE sala8(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala8 (codigo) VALUES ('F9');

CREATE TABLE sala9(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala9 (codigo) VALUES ('F9');

CREATE TABLE sala10(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala10 (codigo) VALUES ('F9');

CREATE TABLE sala11(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala11 (codigo) VALUES ('F9');

CREATE TABLE sala12(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala12 (codigo) VALUES ('F9');

CREATE TABLE sala13(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala13 (codigo) VALUES ('F9');

CREATE TABLE sala14(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala14 (codigo) VALUES ('F9');

CREATE TABLE sala15(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala15 (codigo) VALUES ('F9');

CREATE TABLE sala16(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala16 (codigo) VALUES ('F9');

CREATE TABLE sala17(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala17 (codigo) VALUES ('F9');

CREATE TABLE sala18(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala18 (codigo) VALUES ('F9');

CREATE TABLE sala19(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala19 (codigo) VALUES ('F9');

CREATE TABLE sala20(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala20 (codigo) VALUES ('F9');

CREATE TABLE sala21(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala21 (codigo) VALUES ('F9');

CREATE TABLE sala22(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala22 (codigo) VALUES ('F9');

CREATE TABLE sala23(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala23 (codigo) VALUES ('F9');

CREATE TABLE sala24(
    codigo VARCHAR(3) UNIQUE
);
INSERT INTO sala24 (codigo) VALUES ('F9');

COMMIT;
